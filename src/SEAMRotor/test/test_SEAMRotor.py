
import numpy as np
import unittest
from SEAMRotor.SEAMRotor import SEAMBladeStructure

def configure():

    top = SEAMBladeStructure()
    top.blade_sections = 21
    top.lifetime_cycles = 1e7
    top.wohler_exponent_blade_flap = 10.0
    top.PMtarget = 1.0

    top.rotor_diameter = 177. #[m]
#    top.RootChord = 2.0 #[m]
#    top.MaxChord = 2.5 #[m]
    top.MaxChordrR = 0.2 #[m]

    top.blade_root_flap_max = 47225.
    top.blade_root_edge_max = 26712.
    top.blade_root_flap_leq = 26975.
    top.blade_root_edge_leq = 24252.

    top.sc_frac_flap = 0.3 # sparcap fraction of chord flap
    top.sc_frac_edge = 0.8 # sparcap fraction of thickness edge

    top.safety_factor_blade = 1.1 #[factor]
    top.stress_limit_extreme_blade = 200.0
    top.stress_limit_fatigue_blade = 27.

    top.AddWeightFactorBlade = 1.2 # Additional weight factor for blade shell
    top.blade_material_density = 2100. # [kg / m^3]

    return top

class SEAMRotorTestCase(unittest.TestCase):

    def setUp(self):
        pass

    def test_mass(self):

        top = configure()
        top.run()

        self.assertAlmostEqual(top.blade_mass, 33485.1942627, places = 6)


if __name__ == "__main__":

    unittest.main()


    # data = np.loadtxt('Rotor_outputfile.txt')
    #
    # # Comparing found values for thickness with Kenneth's
    # datacompare_text_flap = (abs(data[:, 5]-top.text_flap))
    # datacompare_text_edge = (abs(data[:, 7]-top.text_edge))
    # datacompare_tfat_flap = (abs(data[:, 9]-top.tfat_flap))
    # datacompare_tfat_edge = (abs(data[:,11]-top.tfat_edge))
    #
    # import matplotlib.pylab as plt
    #
    # # Plot illustrating the absolute differences for the thicknesses as function of the radius
    # plt.figure(1)
    # plt.plot(top.r, datacompare_text_flap, 'o-', label = 'text_flap')
    # plt.plot(top.r, datacompare_text_edge, 'o-', label = 'text_edge')
    # plt.plot(top.r, datacompare_tfat_flap, 'o-', label = 'tfat_flap')
    # plt.plot(top.r, datacompare_tfat_edge, 'o-', label = 'tfat_edge')
    #
    # plt.legend()
    # plt.title("abs differences between thickness's as function of radius")
    # plt.axis([0, 90, 0, 0.0003])
    # plt.xlabel('radius [m]')
    # plt.ylabel("abs difference between thickness's")
    # plt.grid()
    # plt.show()
    #
    # # Plot illustrating the 4 thicknesess's as function of the radius
    # plt.figure(2)
    # plt.plot(top.r, top.text_flap, '>-', label = 'text_flap')
    # plt.plot(top.r, top.text_edge, '>-', label = 'text_edge')
    # plt.plot(top.r, top.tfat_flap, '>-', label = 'tfat_flap')
    # plt.plot(top.r, top.tfat_edge, '>-', label = 'tfat_edge')
    #
    # plt.legend()
    # plt.title('Thickness\'s as function of radius')
    # plt.xlabel('radius [m]')
    # plt.ylabel('spar thickness [m]')
    # plt.grid()
    #
    # plt.show()
